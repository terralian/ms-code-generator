package io.gitee.terralian.code.generator.service.preview.entity;

import java.util.List;

import lombok.Data;

@Data
public class Preview {
    private String baseUrl;
    private List<PreviewData> data;
}
