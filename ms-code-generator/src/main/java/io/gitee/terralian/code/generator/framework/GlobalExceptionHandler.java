package io.gitee.terralian.code.generator.framework;

import java.sql.SQLException;

import io.gitee.terralian.code.generator.framework.entity.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(value = SQLException.class)
    public Result<String> handleSqlException(SQLException sqlException) {
        log.error("", sqlException);
        return Result.failMsg(sqlException.getMessage());
    }

    @ExceptionHandler(value = RuntimeException.class)
    public Result<String> handleRuntimeException(RuntimeException exception) {
        log.error("", exception);
        return Result.failMsg(exception.getMessage());
    }
}
