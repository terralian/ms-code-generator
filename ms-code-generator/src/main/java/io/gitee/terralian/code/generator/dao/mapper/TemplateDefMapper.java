package io.gitee.terralian.code.generator.dao.mapper;

import io.gitee.terralian.code.generator.dao.entity.TemplateDef;
import com.mybatisflex.core.BaseMapper;

public interface TemplateDefMapper extends BaseMapper<TemplateDef> {
}
