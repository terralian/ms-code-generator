package io.gitee.terralian.code.generator.dao.service;

import io.gitee.terralian.code.generator.dao.entity.DataBase;
import io.gitee.terralian.code.generator.dao.mapper.DataBaseMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class DataBaseService extends ServiceImpl<DataBaseMapper, DataBase> {
}
