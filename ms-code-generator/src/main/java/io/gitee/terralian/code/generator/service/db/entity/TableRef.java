package io.gitee.terralian.code.generator.service.db.entity;

import lombok.Data;

@Data
public class TableRef {
    private String tableName;
    private String tableComment;
}
