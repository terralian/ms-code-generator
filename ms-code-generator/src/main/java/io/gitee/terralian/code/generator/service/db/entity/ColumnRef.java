package io.gitee.terralian.code.generator.service.db.entity;

import lombok.Data;

@Data
public class ColumnRef {
    private String tableName;
    private String columnName;
    private Integer ordinalPosition;
    private String isNullable;
    private String dataType;
    private String columnKey;
    private String columnComment;
}
