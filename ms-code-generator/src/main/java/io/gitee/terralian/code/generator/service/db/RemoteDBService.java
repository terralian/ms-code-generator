package io.gitee.terralian.code.generator.service.db;

import io.gitee.terralian.code.generator.dao.entity.DataBase;
import io.gitee.terralian.code.generator.framework.entity.Result;
import io.gitee.terralian.code.generator.service.db.entity.ColumnRef;
import io.gitee.terralian.code.generator.service.db.entity.TableRef;

import java.util.List;

public interface RemoteDBService {

    Result<String> connectTest(String id);

    Result<String> connectTest(DataBase dataBase);

    List<TableRef> getTables(String id);

    List<ColumnRef> getColumns(String id, String tableName);

    List<ColumnRef> getColumns(DataBase dataBase, String tableName);
}
