package io.gitee.terralian.code.generator.controller.request;

import java.util.List;

import io.gitee.terralian.code.generator.service.preview.entity.PreviewData;
import lombok.Data;

@Data
public class PreviewGenerateRequest {
    private String baseUrl;
    private List<PreviewData> data;
}
