package io.gitee.terralian.code.generator.dao.mapper;

import io.gitee.terralian.code.generator.dao.entity.TemplateFile;
import com.mybatisflex.core.BaseMapper;

public interface TemplateFileMapper extends BaseMapper<TemplateFile> {
}
