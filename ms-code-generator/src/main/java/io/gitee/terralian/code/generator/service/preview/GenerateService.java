package io.gitee.terralian.code.generator.service.preview;

import io.gitee.terralian.code.generator.controller.request.PreviewGenerateRequest;

public interface GenerateService {

    void generate(PreviewGenerateRequest request);
}
