package io.gitee.terralian.code.generator.controller.request;

import io.gitee.terralian.code.generator.service.db.entity.TableRef;
import io.gitee.terralian.code.generator.service.template.entity.GenerateConfig;
import lombok.Data;

import java.util.List;

@Data
public class TemplateGenerateRequest {
    private GenerateConfig config;
    private String dbId;
    private List<TableRef> tableRefs;
    private String templateType;
    private List<String> templateIds;
}
