package io.gitee.terralian.code.generator.dao.service;

import io.gitee.terralian.code.generator.dao.entity.TemplateDef;
import io.gitee.terralian.code.generator.dao.mapper.TemplateDefMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class TemplateDefService extends ServiceImpl<TemplateDefMapper, TemplateDef> {
}
