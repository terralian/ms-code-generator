package io.gitee.terralian.code.generator.conf;

import io.gitee.terralian.code.generator.MSCGApplication;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class SqliteCondition implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        return !MSCGApplication.usePropertiesDBConf;
    }
}
