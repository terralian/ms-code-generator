package io.gitee.terralian.code.generator.service.preview.entity;

import lombok.Data;

@Data
public class PreviewData {
    private String absoluteUrl;
    private String parentBaseUrl;
    private String codeBaseUrl;
    private String packageName;
    private String packageUrl;
    private String fileName;
    private String data;
}
