package io.gitee.terralian.code.generator.dao.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("database")
public class DataBase {

    @Id
    private String id;
    private String name;
    private String url;
    private String username;
    private String password;
    private String driverClassName;
    private String dbName;
}
