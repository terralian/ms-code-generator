package io.gitee.terralian.code.generator.dao.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("template_def")
public class TemplateDef {
    @Id
    private String id;
    private String codeBaseUrlTemp;
    private String packageNameTemp;
    private String fileNameTemp;
    private String data;
}
