package io.gitee.terralian.code.generator.dao.service;

import io.gitee.terralian.code.generator.dao.entity.TemplateFile;
import io.gitee.terralian.code.generator.dao.mapper.TemplateFileMapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class TemplateFileService extends ServiceImpl<TemplateFileMapper, TemplateFile> {

    public String joinPath(String parentPath, String id) {
        if (parentPath == null) {
            return id + "-";
        }
        return parentPath + id + "-";
    }
}
