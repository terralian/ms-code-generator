package io.gitee.terralian.code.generator.framework.conf;

import cn.hutool.core.util.StrUtil;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class MyBatisFlexConf implements MyBatisFlexCustomizer {

    public MyBatisFlexConf() {
        //开启审计功能
        AuditManager.setAuditEnable(true);
        //设置 SQL 审计收集器
        AuditManager.setMessageCollector(auditMessage -> log.info("{}ms -> {}", auditMessage.getElapsedTime(),
                StrUtil.removeAll(auditMessage.getFullSql(), "`")));
    }

    @Override
    public void customize(FlexGlobalConfig flexGlobalConfig) {
        FlexGlobalConfig.KeyConfig keyConfig = new FlexGlobalConfig.KeyConfig();
        keyConfig.setKeyType(KeyType.Generator);
        keyConfig.setValue("snowFlakeId");
        keyConfig.setBefore(true);
        flexGlobalConfig.setKeyConfig(keyConfig);
    }
}
