package io.gitee.terralian.code.generator.conf;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 静态全局配置，可在该配置中添加预变量，使用标志决定数据源等
 */
public class StaticGlobalConf {
    /**
     * Java类型映射
     */
    public static Map<String, String> javaTypeMap;
    /**
     * Java类型包导入
     */
    public static Map<String, String> javaTypePackageMap;

    /**
     * 预定义变量
     */
    public static Map<String, String> params = new LinkedHashMap<>();

    static {
        javaTypeMap = new LinkedHashMap<>();
        javaTypeMap.put("bigint", "Long");
        javaTypeMap.put("binary", "byte[]");
        javaTypeMap.put("bit", "byte");
        javaTypeMap.put("blob", "byte[]");
        javaTypeMap.put("char", "String");
        javaTypeMap.put("date", "LocalDate");
        javaTypeMap.put("datetime", "LocalDateTime");
        javaTypeMap.put("decimal", "BigDecimal");
        javaTypeMap.put("double", "Double");
        javaTypeMap.put("float", "Float");
        javaTypeMap.put("int", "Integer");
        javaTypeMap.put("integer", "Integer");
        javaTypeMap.put("json", "String");
        javaTypeMap.put("longblob", "byte[]");
        javaTypeMap.put("longtext", "String");
        javaTypeMap.put("mediumblob", "byte[]");
        javaTypeMap.put("mediumtext", "String");
        javaTypeMap.put("text", "String");
        javaTypeMap.put("timestamp", "Date");
        javaTypeMap.put("tinyblob", "byte[]");
        javaTypeMap.put("tinyint", "Integer");
        javaTypeMap.put("tinytext", "String");
        javaTypeMap.put("varbinary", "byte[]");
        javaTypeMap.put("varchar", "String");
        javaTypeMap.put("year", "Integer");
    }

    static {
        javaTypePackageMap = new LinkedHashMap<>();
        javaTypePackageMap.put("LocalDate", "import java.time.LocalDate;");
        javaTypePackageMap.put("LocalDateTime", "import java.time.LocalDateTime;");
        javaTypePackageMap.put("BigDecimal", "import java.math.BigDecimal;");
    }

    /**
     * 添加静态预变量参数
     *
     * @param key 键
     * @param value 值
     */
    public static void addParams(String key, String value) {
        params.put(key, value);
    }

    /**
     * 添加静态预变量参数
     *
     * @param key 键
     * @param value 值
     */
    public static void addParams(CommonKey key, String value) {
        params.put(key.name(), value);
    }
}
