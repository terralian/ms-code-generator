package io.gitee.terralian.code.generator.dao.mapper;

import io.gitee.terralian.code.generator.dao.entity.DataBase;
import com.mybatisflex.core.BaseMapper;

public interface DataBaseMapper extends BaseMapper<DataBase> {
}
