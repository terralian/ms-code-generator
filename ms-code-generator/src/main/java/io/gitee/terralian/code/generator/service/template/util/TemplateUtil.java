package io.gitee.terralian.code.generator.service.template.util;

import cn.hutool.core.util.StrUtil;

public abstract class TemplateUtil {


    /**
     * 将包名转换为路径
     *
     * @param packageName 包名
     */
    public static String packageNameToUrl(String packageName) {
        if (StrUtil.isEmpty(packageName)) {
            return packageName;
        }
        return packageName.replaceAll("\\.", "/");
    }

}
