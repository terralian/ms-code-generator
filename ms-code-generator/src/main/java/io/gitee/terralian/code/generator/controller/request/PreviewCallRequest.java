package io.gitee.terralian.code.generator.controller.request;

import java.util.List;
import java.util.Map;

import io.gitee.terralian.code.generator.service.db.entity.TableRef;
import lombok.Data;

@Data
public class PreviewCallRequest {
    private String dbId;
    private List<TableRef> tableRefs;
    private List<String> templateFileIds;
    private String baseUrl;
    private String tableNameReplaceIdentifier;
    private Map<String, Object> params;
}
