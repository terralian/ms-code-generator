package io.gitee.terralian.code.generator;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("io.gitee.terralian.**.dao.mapper")
@SpringBootApplication(scanBasePackages = {"io.gitee.terralian.code.generator"})
public class MSCGApplication {

    /**
     * 是否使用项目原生配置中的数据源设置，默认为false。
     * <p>
     * 默认使用内置的sqlite作为数据源，将会在根目录下新增文件：/cache/msc.sqlite
     */
    public static boolean usePropertiesDBConf = false;

    public static void main(String[] args) {
        System.setProperty("spring.web.resources.static-locations", "classpath:/ui/");
        SpringApplication.run(MSCGApplication.class, args);

    }
}
