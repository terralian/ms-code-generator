package io.gitee.terralian.code.generator.service.util;

import cn.hutool.core.util.StrUtil;

public abstract class UrlHelper {

    public static String format(String url) {
        if (url.endsWith("/")) {
            url = url.substring(0, url.length() - 1);
        }
        if (url.startsWith("/") && url.length() > 1) {
            url = url.substring(1);
        }
        return url;
    }

    public static String toUrl(String packageName) {
        return StrUtil.replace(packageName, ".", "/");
    }

}
