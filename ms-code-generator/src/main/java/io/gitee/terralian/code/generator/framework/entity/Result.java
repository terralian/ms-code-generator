package io.gitee.terralian.code.generator.framework.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用返回响应
 *
 * @author terra.lian
 */
@Data
public class Result<T> implements Serializable {

    /**
     * 是否成功，不关心失败的具体类型
     */
    private Boolean success;
    /**
     * 成功或失败的信息
     */
    private String message;
    /**
     * 数据
     */
    private T data;

    /**
     * 返回一个仅有信息的成功返回值
     *
     * @param message 成功的信息
     */
    public static <T> Result<T> successMsg(String message) {
        return success(message, null);
    }

    /**
     * 返回一个仅有数据的成功返回值
     *
     * @param data 数据
     * @param <T> 数据类型
     */
    public static <T> Result<T> success(T data) {
        return success(null, data);
    }

    /**
     * 返回一个默认的成功信息，不关心信息和结果
     *
     * @param <T> 数据类型
     * @param ignore 无论传入任何参数都会忽略
     */
    public static <T> Result<T> defaultSuccess(Object... ignore) {
        return success(null, null);
    }

    /**
     * 返回一个包含信息和数据的成功返回值
     *
     * @param message 成功的信息
     * @param data 数据
     * @param <T> 数据类型
     */
    public static <T> Result<T> success(String message, T data) {
        Result<T> result = new Result<>();
        result.setMessage(message);
        result.setData(data);
        result.setSuccess(true);
        return result;
    }

    /**
     * 返回一个仅有信息的失败返回值
     *
     * @param message 失败的信息
     */
    public static <T> Result<T> failMsg(String message) {
        return fail(message, null);
    }

    /**
     * 返回一个仅有数据的失败返回值
     *
     * @param data 数据
     * @param <T> 数据类型
     */
    public static <T> Result<T> fail(T data) {
        return fail(null, data);
    }

    /**
     * 返回一个包含信息和数据的失败返回值
     *
     * @param message 成功的信息
     * @param data 数据
     * @param <T> 数据类型
     */
    public static <T> Result<T> fail(String message, T data) {
        Result<T> result = new Result<>();
        result.setMessage(message);
        result.setData(data);
        result.setSuccess(false);
        return result;
    }
}
