package io.gitee.terralian.code.generator.service.template.entity;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import lombok.Data;

/**
 * 生成配置
 */
@Data
public class GenerateConfig {

    /**
     * 预定义变量
     */
    private Map<String, Object> variables;

    /**
     * 表名中前后缀忽略识别符
     */
    private String tableNameReplaceIdentifier;
    /**
     * 自动导入的包
     */
    private Set<String> autoImports;

    public GenerateConfig() {
        this.variables = new LinkedHashMap<>();
        this.autoImports = new LinkedHashSet<>();
    }

    /**
     * 设置预定义变量
     *
     * @param key 键
     * @param value 值
     */
    public void setVariable(String key, Object value) {
        variables.put(key, value);
    }


    /**
     * 获取预定义变量
     *
     * @param key 键
     * @param <T> 类型
     */
    @SuppressWarnings("unchecked")
    public <T> T getVariable(String key) {
        return (T) variables.get(key);
    }

    /**
     * 获取预定义变量（或空）
     *
     * @param key 键
     */
    public String getStringEmpty(String key) {
        Object value = variables.get(key);
        if (value == null) return "";
        return value.toString();
    }

    public void addPackageImport(String packageImport) {
        this.autoImports.add(packageImport);
    }
}
