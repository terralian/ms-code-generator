package io.gitee.terralian.code.generator.conf;

/**
 * 一些常用定义的Key，这些key非系统强制的，但是可以经常在模板里使用
 */
public enum CommonKey {

    /**
     * 项目，可以类似前缀，后缀
     */
    project,

    /**
     * 用户名
     */
    author,

    /**
     * 包名
     */
    packageName,


    ;
}
