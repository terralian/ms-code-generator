package io.gitee.terralian.code.generator.service.template.entity;

import lombok.Data;

/**
 * 模板字段信息
 */
@Data
public class TemplateColumn {
    /**
     * 表字段名
     */
    private String name;
    /**
     * 驼峰式命名
     */
    private String nameCamelCase;
    /**
     * 帕斯卡命名
     */
    private String namePascal;
    /**
     * 数据库类型
     */
    private String sqlType;
    /**
     * java类型
     */
    private String javaType;
    /**
     * 是否可以为空
     */
    private Boolean isNullable;
    /**
     * 是否主键
     */
    private Boolean isPrimaryKey;
    /**
     * 序号
     */
    private Integer ordinalPosition;
    /**
     * 注释
     */
    private String comment;
}
