package io.gitee.terralian.code.generator.service.template;

import java.io.Writer;

import io.gitee.terralian.code.generator.service.template.entity.GenerateConfig;

public interface TemplateEngine {

    /**
     * 处理模板
     *
     * @param config 配置
     * @param templateData 模板数据
     * @param writer 输出
     */
    void parse(GenerateConfig config, String templateData, Writer writer);

    /**
     * 处理模版
     *
     * @param config 配置
     * @param templateData 模版
     */
    String parse(GenerateConfig config, String templateData);
}
