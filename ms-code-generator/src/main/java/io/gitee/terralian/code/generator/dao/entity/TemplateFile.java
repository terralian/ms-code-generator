package io.gitee.terralian.code.generator.dao.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import lombok.Data;

@Data
@Table("template_file")
public class TemplateFile {
    @Id
    private String id;
    private String fileName;
    private Boolean directory;
    private String parentId;
    private String path;
    private Integer level;
}
