package io.gitee.terralian.code.generator.controller.request;

import lombok.Data;

@Data
public class PreviewOpenFolderRequest {

    private String url;
}
