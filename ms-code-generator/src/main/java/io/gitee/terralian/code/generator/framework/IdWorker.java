package io.gitee.terralian.code.generator.framework;

import com.mybatisflex.core.keygen.IKeyGenerator;
import com.mybatisflex.core.keygen.KeyGeneratorFactory;
import com.mybatisflex.core.keygen.KeyGenerators;

public abstract class IdWorker {

    private static final IKeyGenerator keyGenerator = KeyGeneratorFactory.getKeyGenerator(KeyGenerators.snowFlakeId);

    public static String nextId() {
        return String.valueOf(keyGenerator.generate(null, null));
    }
}
