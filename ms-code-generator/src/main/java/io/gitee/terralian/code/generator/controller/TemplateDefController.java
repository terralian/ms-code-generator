package io.gitee.terralian.code.generator.controller;

import io.gitee.terralian.code.generator.dao.entity.TemplateDef;
import io.gitee.terralian.code.generator.dao.service.TemplateDefService;
import io.gitee.terralian.code.generator.framework.entity.Result;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("template/def")
@AllArgsConstructor
public class TemplateDefController {

    private final TemplateDefService templateDefService;

    @GetMapping("{id}")
    public Result<TemplateDef> getById(@PathVariable("id") String id) {
        TemplateDef templateDef = templateDefService.getById(id);
        return Result.success(templateDef);
    }

    @PutMapping("update")
    public Result<Object> update(@RequestBody TemplateDef templateDef) {
        templateDefService.updateById(templateDef);
        return Result.defaultSuccess();
    }
}
