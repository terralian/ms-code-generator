package io.gitee.terralian.code.generator.service.preview;

import io.gitee.terralian.code.generator.controller.request.PreviewCallRequest;
import io.gitee.terralian.code.generator.service.preview.entity.Preview;

public interface PreviewService {

    Preview call(PreviewCallRequest request);
}
