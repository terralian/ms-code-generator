package io.gitee.terralian.code.generator.controller;

import java.util.ArrayList;
import java.util.List;

import io.gitee.terralian.code.generator.dao.entity.TemplateDef;
import io.gitee.terralian.code.generator.dao.entity.TemplateFile;
import io.gitee.terralian.code.generator.dao.entity.table.Tables;
import io.gitee.terralian.code.generator.dao.service.TemplateDefService;
import io.gitee.terralian.code.generator.dao.service.TemplateFileService;
import io.gitee.terralian.code.generator.framework.IdWorker;
import io.gitee.terralian.code.generator.framework.entity.Result;
import cn.hutool.core.collection.CollectionUtil;
import lombok.AllArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("template/file")
@AllArgsConstructor
public class TemplateFileController {

    private final TemplateFileService templateFileService;
    private final TemplateDefService templateDefService;

    @GetMapping("list")
    public Result<List<TemplateFile>> list() {
        return Result.success(templateFileService.list());
    }

    @PostMapping("save")
    public Result<Object> save(@RequestBody TemplateFile file) {
        if (file.getParentId() == null) {
            file.setParentId("-1");
        } else if (!"-1".equals(file.getParentId())) {
            TemplateFile parentInfo = templateFileService.queryChain()
                    .where(Tables.templateFile.id.eq(file.getParentId()))
                    .one();
            Assert.notNull(parentInfo, "父目录不存在");
            Assert.isTrue(parentInfo.getDirectory(), "父级不是一个目录");
        }
        file.setId(IdWorker.nextId());
        file.setPath(templateFileService.joinPath(file.getPath(), file.getId()));
        templateFileService.save(file);
        if (!file.getDirectory()) {
            TemplateDef templateDef = new TemplateDef();
            templateDef.setId(file.getId());
            templateDefService.save(templateDef);
        }
        return Result.defaultSuccess();
    }

    @PutMapping("rename")
    public Result<Object> rename(@RequestBody TemplateFile file) {
        TemplateFile updateModal = new TemplateFile();
        updateModal.setId(file.getId());
        updateModal.setFileName(file.getFileName());
        templateFileService.updateById(updateModal);
        return Result.defaultSuccess();
    }

    @Transactional
    @DeleteMapping("delete")
    public Result<String> delete(@RequestParam("path") String path) {
        List<TemplateFile> files = templateFileService.queryChain()
                .where(Tables.templateFile.path.likeLeft(path))
                .list();
        if (CollectionUtil.isEmpty(files)) {
            return Result.failMsg("未找到目录");
        }

        List<String> fileIds = new ArrayList<>();
        List<String> defIds = new ArrayList<>();
        for (TemplateFile file : files) {
            fileIds.add(file.getId());
            if (!file.getDirectory()) {
                defIds.add(file.getId());
            }
        }
        templateFileService.removeByIds(fileIds);
        if (CollectionUtil.isNotEmpty(defIds)) {
            templateDefService.removeByIds(defIds);
        }
        return Result.defaultSuccess();
    }

}
