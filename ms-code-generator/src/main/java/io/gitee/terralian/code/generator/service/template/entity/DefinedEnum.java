package io.gitee.terralian.code.generator.service.template.entity;

import io.gitee.terralian.code.generator.service.db.entity.ColumnRef;

/**
 * 预定义变量
 */
public enum DefinedEnum {

    /**
     * 项目根路径，如D:/Project/example
     */
    baseUrl,

    /**
     * 代码根路径，相对路径（baseUrl），如: /order/src/main/java
     */
    codeBaseUrl,

    /**
     * 包路径，相对于代码根路径，如/com/foo/bar
     */
    packageUrl,

    /**
     * 包名，如com.foo.bar
     */
    packageName,

    /**
     * 文件名，通过文件名模板生成
     */
    fileName,

    /**
     * 自动导入的Java类包依赖
     */
    autoImports,

    /**
     * 表名
     */
    tableName,

    /**
     * 表注释
     */
    tableComment,

    /**
     * 表名转帕斯卡命名
     */
    EntityName,

    /**
     * 表名转驼峰命名
     */
    entityName,

    /**
     * 字段信息
     *
     * @see ColumnRef
     */
    columns,

    /**
     * 当前系统日期
     */
    date,

    /**
     * 当前年份
     */
    year,

    /**
     * 当前月份
     */
    month,

    /**
     * 当前天数
     */
    dayOfMonth,

    /**
     * 当前小时
     */
    hour,

    /**
     * 当前分钟
     */
    minute,

    /**
     * 当前秒
     */
    second,
}
