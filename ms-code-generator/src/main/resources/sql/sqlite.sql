CREATE TABLE "database" (
  "id" text(64) NOT NULL,
  "name" text(255) NOT NULL,
  "url" text(1024) NOT NULL,
  "username" text(255),
  "password" text(255),
  "driver_class_name" text(50) NOT NULL,
  "db_name" text(255),
  PRIMARY KEY ("id")
);

CREATE TABLE "template_def" (
  "id" text(64) NOT NULL,
  "code_base_url_temp" text(255),
  "package_name_temp" text(255),
  "file_name_temp" text(255),
  "data" text,
  PRIMARY KEY ("id")
);

CREATE TABLE "template_file" (
  "id" TEXT(64) NOT NULL,
  "file_name" text(255),
  "directory" integer(1),
  "parent_id" INTEGER(64),
  "path" TEXT(693),
  "level" integer(1),
  PRIMARY KEY ("id")
);

