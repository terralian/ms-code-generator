# ms-code-generator
通用模版代码生成器



## ✨ Features

- 基于react + spring boot
- 可使用sqlite存储，直接作为项目的一部分配置进行代码协同
- 使用velocity作为模版引擎



## 🖥 项目要求

- maven下的java项目
- 使用mysql数据库



## 📦 使用

在pom内引入依赖

```xml
<dependency>
    <groupId>io.gitee.terralian</groupId>
    <artifactId>ms-code-generator</artifactId>
    <version>1.0.2</version>
    <scope>test</scope>
</dependency>
```

在`src/main/test`下编写启动类

```java
import io.gitee.terralian.code.generator.MSCGApplication;
import io.gitee.terralian.code.generator.conf.CommonKey;
import io.gitee.terralian.code.generator.conf.StaticGlobalConf;

public class CodeGenerator {
    public static void main(String[] args) {
        StaticGlobalConf.addParams(CommonKey.author, "作者");
        MSCGApplication.main(args);
    }
}
```

启动后访问

```
http://localhost:8080/
```



## 🔎 预览

![3](assets/1.png)