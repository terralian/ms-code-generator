import { ProxyOptions } from "vite";

/**
 * 在生产环境 代理是无法生效的，所以这里没有生产环境的配置
 * -------------------------------
 * The agent cannot take effect in the production environment
 * so there is no configuration of the production environment
 * For details, please see
 * https://pro.ant.design/docs/deploy
 */

const devServer = "http://127.0.0.1:8080";

/**
 * 获取代理设置
 * @param env 环境配置
 */
export const getProxy = (env: any) => {
  return {
    [env.VITE_BASE_API]: {
      target: devServer,
      changeOrigin: true,
      rewrite: (path) => path.replace(env.VITE_BASE_API, ""),
    } as ProxyOptions,
  };
};
