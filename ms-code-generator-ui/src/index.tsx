import App from "@/app";
import { ArcoDesignConfig, ConfigurationRegisterScanner } from "@/conf";
import "@/index.less";
import { ConfigProvider } from "@arco-design/web-react";
import "@arco-design/web-react/dist/css/arco.css";
import "@icon-park/react/styles/index.css";
import { createRoot } from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { RecoilRoot } from "recoil";
import "./theme.less";

ConfigurationRegisterScanner.scan();

const container = document.getElementById("root")!;
const root = createRoot(container);
root.render(
  <BrowserRouter>
    <RecoilRoot>
      <ConfigProvider componentConfig={ArcoDesignConfig}>
        <App />
      </ConfigProvider>
    </RecoilRoot>
  </BrowserRouter>
);
