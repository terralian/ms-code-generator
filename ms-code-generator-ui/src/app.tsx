import { AppSteps } from "./components/steps";
import { PageTabs } from "./pages";

const App = () => {
  return (
    <>
      <AppSteps />
      <PageTabs />
    </>
  );
};

export default App;
