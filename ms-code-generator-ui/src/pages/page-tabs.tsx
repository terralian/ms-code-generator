import { AppStepAtom } from "@/components/steps";
import { useStore } from "@/util/hooks";
import { Tabs } from "@arco-design/web-react";
import { DBPage } from "./db";
import { PreviewPage } from "./preview/page";
import "./style.less";
import { TemplatePage } from "./template/page";

export const PageTabs = () => {
  const { value } = useStore(AppStepAtom);

  return (
    <Tabs
      renderTabHeader={() => <></>}
      activeTab={value.id}
      className="page-tabs"
    >
      <Tabs.TabPane title key="db">
        <DBPage />
      </Tabs.TabPane>
      <Tabs.TabPane title key="template">
        <TemplatePage />
      </Tabs.TabPane>
      <Tabs.TabPane title key="preview">
        <PreviewPage />
      </Tabs.TabPane>
    </Tabs>
  );
};
