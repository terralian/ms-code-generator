import {
  PreviewEditor,
  PreviewHeader,
  PreviewTree,
  PreviewParamForm,
} from "@/components";
import { HeightUtil } from "@/util";
import { ResizeBox } from "@arco-design/web-react";

export const PreviewPage = () => {
  const content = (
    <ResizeBox.Split
      style={{
        height: HeightUtil.fillHeightStyle(2),
        userSelect: "none",
      }}
      direction="horizontal-reverse"
      size="300px"
      min="230px"
      panes={[
        <PreviewParamForm />,
        <PreviewEditor height={HeightUtil.fillHeightStyle(2)} />,
      ]}
    ></ResizeBox.Split>
  );
  return (
    <>
      <PreviewHeader />
      <ResizeBox.Split
        style={{
          height: HeightUtil.fillHeightStyle(2),
          userSelect: "none",
        }}
        direction="horizontal"
        size="300px"
        min="230px"
        panes={[<PreviewTree />, content]}
      />
    </>
  );
};
