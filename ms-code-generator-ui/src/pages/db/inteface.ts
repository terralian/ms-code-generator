import { TableRef } from "@/api";

export interface DBPageStore {
  dbId?: string;
  tables?: TableRef[];
}
