import { TableRef, db } from "@/api";
import { HeightUtil, ResultUtil } from "@/util";
import { createAtom, useStore } from "@/util/hooks";
import { ResizeBox } from "@arco-design/web-react";
import { useEffect } from "react";
import { DBTable, TableRefTable, TableRefTableAtom } from "@/components";
import { DBPageStore } from "./inteface";

export const DBPageAtom = createAtom<DBPageStore>({});

export const DBPage = () => {
  const dbTableStore = useStore(TableRefTableAtom);

  async function updateDB(dbId?: string) {
    let tables: TableRef[] = [];
    if (dbId) {
      const res = await db.tables(dbId!);
      if (ResultUtil.showResult(res)) {
        tables = res.data;
      }
    }
    dbTableStore.updateStore({
      data: tables,
    });
  }

  useEffect(() => {
    updateDB("");
  }, []);

  return (
    <ResizeBox.Split
      style={{
        height: HeightUtil.fillHeightStyle(1),
        userSelect: "none",
      }}
      direction="horizontal"
      size="300px"
      min="230px"
      panes={[<DBTable />, <TableRefTable />]}
    />
  );
};
