import { createAtom } from "@/util/hooks";
import { DBPageStore } from "./inteface";

export const DBPageAtom = createAtom<DBPageStore>({});
