import {
  TemplateDefEditor,
  TemplateDefHeader,
  TemplateFileForm,
  TemplateFileTree,
  TemplateTreeAtom,
} from "@/components";
import { HeightUtil } from "@/util";
import { useStore } from "@/util/hooks";
import { Empty, ResizeBox } from "@arco-design/web-react";

const heightStyle = HeightUtil.fillHeightStyle(1);
const contentHeightStyle = HeightUtil.fillHeightStyle(2);

export const TemplatePage = () => {
  const fileTreeAtom = useStore(TemplateTreeAtom);
  const { isFileSelected } = fileTreeAtom.value;

  const contentBox = (
    <div>
      <TemplateDefHeader />
      <ResizeBox.Split
        style={{ height: contentHeightStyle, userSelect: "none" }}
        direction="horizontal-reverse"
        size="300px"
        min="230px"
        panes={[
          <TemplateFileForm />,
          <TemplateDefEditor height={contentHeightStyle} />,
        ]}
      />
    </div>
  );

  const emtpyHolder = (
    <div>
      <TemplateDefHeader showAction={false} />
      <Empty
        description="请选择一个模版"
        style={{ height: contentHeightStyle, paddingTop: 100 }}
      />
    </div>
  );

  return (
    <ResizeBox.Split
      style={{ height: heightStyle, userSelect: "none" }}
      direction="horizontal"
      size="300px"
      min="230px"
      panes={[<TemplateFileTree />, isFileSelected ? contentBox : emtpyHolder]}
    />
  );
};
