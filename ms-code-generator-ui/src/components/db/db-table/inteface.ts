import { Database } from "@/api";

export interface DBTableStore {
  data: Database[];
  selected?: Database;
}
