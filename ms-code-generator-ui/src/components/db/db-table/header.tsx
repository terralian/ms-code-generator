import { Header } from "@/components";
import { useStore } from "@/util/hooks";
import { Button } from "@arco-design/web-react";
import { Plus } from "@icon-park/react";
import { DBTableModalAtom } from "./modal";

export const DBTableHeader = () => {
  const dbTableModalStore = useStore(DBTableModalAtom);

  function onAddButtonClick() {
    dbTableModalStore.updateStore({
      open: true,
    });
  }

  return (
    <Header
      title="数据源"
      extra={<Button icon={<Plus />} onClick={onAddButtonClick} />}
    />
  );
};
