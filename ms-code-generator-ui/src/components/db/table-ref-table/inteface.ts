import { TableRef } from "@/api";

export interface TableRefTableStore {
  data: TableRef[];
  selectedRowKeys?: (string | number)[];
}
