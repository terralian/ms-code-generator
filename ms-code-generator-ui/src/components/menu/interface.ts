import { DropdownProps, MenuItemProps } from "@arco-design/web-react";

export interface ContextMenuItemProps extends MenuItemProps {
  title?: string;
  icon?: React.ReactNode;
  hidden?: boolean;
}

export interface ContextMenuProps extends DropdownProps {
  menus: ContextMenuItemProps[];
}
