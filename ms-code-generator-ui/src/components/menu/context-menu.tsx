import { Dropdown, Menu } from "@arco-design/web-react";
import { PropsWithChildren } from "react";
import { ContextMenuProps } from ".";

const iconStyle = {
  marginRight: 8,
  fontSize: 16,
  transform: "translateY(1px)",
};

export const ContextMenu: React.FC<PropsWithChildren<any>> = ({
  children,
  menus: menuDataList,
  ...props
}: ContextMenuProps) => {
  const menus = (
    <Menu style={{ width: 200 }}>
      {menuDataList
        .filter((k) => !k.hidden)
        .map(({ icon, title, ...kprops }) => {
          return (
            <Menu.Item {...kprops}>
              <span style={iconStyle}>{icon}</span>
              {title}
            </Menu.Item>
          );
        })}
    </Menu>
  );
  return (
    <Dropdown trigger="contextMenu" position="bl" {...props} droplist={menus}>
      {children}
    </Dropdown>
  );
};
