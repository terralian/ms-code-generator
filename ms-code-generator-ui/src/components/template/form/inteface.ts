import { TemplateDef } from "@/api";
import { FormProps } from "@arco-design/web-react";

export interface TemplateFileFormProps extends FormProps {
  height?: string;
}

export interface TemplateFileFormStore {
  data?: TemplateDef;
  requestFormUpdate: boolean;
}
