import { TemplateFile } from "@/api";
import { ModalStore } from "@/components";
import { TreeDataType } from "@arco-design/web-react/es/Tree/interface";

export interface TemplateFileTreeStore {
  originData: TemplateFile[];
  data: TreeDataType[];
  selectedKey?: string;
  isFileSelected: boolean;
  checkedKeys?: string[];
  clickedKey?: string;
}

export interface TemplateFileModalStore extends ModalStore<TemplateFile> {
  directory: boolean;
}

export interface TemplateRenameModalStore extends ModalStore<TemplateFile> {
  directory: boolean;
}
