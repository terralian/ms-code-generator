import { TemplateFile } from "@/api";

export function getFileRecord(selectedKey: string, data: TemplateFile[]) {
  if (!selectedKey) return null;
  for (const item of data) {
    if (item.id === selectedKey) {
      return item;
    }
  }
}
