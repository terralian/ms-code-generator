import { Header, TemplateFileUtil, TemplateTreeAtom } from "@/components";
import { useStore } from "@/util/hooks";
import { Button, Message } from "@arco-design/web-react";
import { FileAdditionOne } from "@icon-park/react";
import { TemplateFileModalAtom } from "./modal-file";

export const TemplateTreeHeader = () => {
  const fileModalStore = useStore(TemplateFileModalAtom);
  const treeStore = useStore(TemplateTreeAtom);
  const { clickedKey, originData } = treeStore.value;
  const record = TemplateFileUtil.getFileRecord(clickedKey!, originData);

  function onAddButtonClick() {
    if (record && !record.directory) {
      Message.error("非目录不能添加子文件");
      return;
    }
    treeStore.updateStore({
      clickedKey: undefined,
    });
    fileModalStore.updateStore({
      open: true,
      directory: false,
      type: "add",
    });
  }

  return (
    <Header
      title="模板"
      extra={<Button icon={<FileAdditionOne />} onClick={onAddButtonClick} />}
    />
  );
};
