import { CodeMirrorEditor } from "@/components";
import { createAtom, useStore } from "@/util/hooks";

interface TemplateDefEditorProps {
  height?: string;
}

interface TemplateDefEditorStore {
  data?: string;
}

export const TemplateDefEditorAtom = createAtom<TemplateDefEditorStore>({});

export const TemplateDefEditor = (props: TemplateDefEditorProps) => {
  const editorStore = useStore(TemplateDefEditorAtom);
  return (
    <CodeMirrorEditor
      height={props.height}
      width="100%"
      value={editorStore.value.data}
      onChange={(value) => {
        editorStore.updateStore({
          data: value,
        });
      }}
    />
  );
};
