import React from "react";
import "./style.less";

export interface HeaderProps {
  title: React.ReactNode;
  extra?: React.ReactNode;
}

export const Header = ({ title, extra }: HeaderProps) => {
  return (
    <div className="header">
      <div className="title arco-table-th-item arco-table-th-item-title">
        {title}
      </div>
      <div className="icon">{extra}</div>
    </div>
  );
};
