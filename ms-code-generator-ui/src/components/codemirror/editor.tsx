import { javascript } from "@codemirror/lang-javascript";
import { darcula } from "@uiw/codemirror-theme-darcula";
import CodeMirror, {
  Extension,
  ReactCodeMirrorRef,
} from "@uiw/react-codemirror";
import { useRef } from "react";
import { CodeMirrorEditorProps } from "./inteface";
import "./style.less";

export const CodeMirrorEditor = (props: CodeMirrorEditorProps) => {
  const editorRef = useRef<ReactCodeMirrorRef>(null);

  const extensions: Extension[] = [javascript()];

  return (
    <CodeMirror
      {...props}
      ref={editorRef}
      theme={darcula}
      basicSetup={{
        foldGutter: false,
        autocompletion: false,
        dropCursor: false,
        drawSelection: true,
        allowMultipleSelections: true,
        indentOnInput: true,
        highlightActiveLine: false,
        searchKeymap: false,
      }}
      extensions={extensions}
    />
  );
};
