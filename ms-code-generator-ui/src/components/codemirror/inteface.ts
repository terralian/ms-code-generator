import { ReactCodeMirrorProps } from "@uiw/react-codemirror";

export interface CodeMirrorEditorProps extends ReactCodeMirrorProps {
  /** 展示最大高度 */
  fullHeight?: boolean;
}
