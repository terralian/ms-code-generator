import { FormInstance } from "@arco-design/web-react";
import { ReactNode } from "react";
import { RecoilState } from "recoil";
import { DraggableModalProps } from "./drag-modal";

export interface ModalStore<T> {
  open: boolean;
  type: "add" | "update" | "detail";
  record?: T;
}

export interface FormModalProps<T> extends DraggableModalProps {
  atom: RecoilState<ModalStore<T> & any>;
  title?: string;
  fullTitle?: string;
  form: FormInstance;
  autoSetField?: boolean;
  children?: ReactNode;
}
