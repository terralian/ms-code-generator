import { createAtom } from "@/util/hooks";
import { ModalStore } from ".";

export function createModalAtom<T>() {
  return createAtom({
    open: false,
    type: "add",
  } as ModalStore<T>);
}
