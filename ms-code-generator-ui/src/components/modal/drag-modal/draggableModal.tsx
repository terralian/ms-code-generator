import { Modal, ModalProps } from "@arco-design/web-react";
import React, { PropsWithChildren, useState } from "react";
import Draggable, { DraggableEventHandler } from "react-draggable";
import "../base-modal.less";
import "./draggableModal.less";

export interface DraggableModalProps extends ModalProps {
  draggable?: boolean;
}

/**
 * 可拖拽弹出框
 */
export const DraggableModal: React.FC<
  PropsWithChildren<DraggableModalProps>
> = ({ draggable = true, className, afterClose, ...props }) => {
  const [position, setPosition] = useState({ x: 0, y: 0 });

  const onDrag: DraggableEventHandler = (e, data) => {
    setPosition({ x: data.x, y: data.y });
  };

  function handleAfterClose() {
    setPosition({ x: 0, y: 0 });
    afterClose?.();
  }

  return (
    <Modal
      className={`base-modal draggable-modal ${className}`}
      alignCenter={false}
      mountOnEnter={false}
      simple
      escToExit
      focusLock
      maskStyle={{
        backgroundColor: "transparent",
        transition: "none",
      }}
      {...props}
      afterClose={handleAfterClose}
      modalRender={(modalNode) => {
        return (
          <Draggable
            handle=".arco-modal-title"
            position={position}
            onDrag={onDrag}
          >
            {modalNode}
          </Draggable>
        );
      }}
    ></Modal>
  );
};
