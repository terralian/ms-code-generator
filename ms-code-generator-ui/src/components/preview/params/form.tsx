import { preview } from "@/api";
import { ResultUtil } from "@/util";
import { createAtom, useStore } from "@/util/hooks";
import { Button, Form, Grid, Input } from "@arco-design/web-react";
import { IconDelete } from "@arco-design/web-react/icon";
import { useEffect } from "react";
import { PreviewParamStore } from "./inteface";

export const PreviewParamFormAtom = createAtom<PreviewParamStore>({});

export const PreviewParamForm = () => {
  const [form] = Form.useForm();
  const store = useStore(PreviewParamFormAtom);

  async function initParams() {
    const res = await preview.getParams();
    if (!ResultUtil.showError(res)) {
      return;
    }

    const { data: originData } = res;

    const data = Object.keys(originData).map((k) => ({
      key: k,
      value: originData[k],
    }));

    store.updateStore({
      data: originData,
    });
    form.setFieldsValue({
      data,
    });
  }

  function updateStoreData(arrayData: Array<{ key: string; value: string }>) {
    const res: Record<string, string> = {};
    for (const item of arrayData) {
      if (!item || !item.key) continue;
      res[item.key] = item.value;
    }
    store.updateStore({
      data: res,
    });
  }

  useEffect(() => {
    initParams();
  }, []);
  return (
    <Form
      form={form}
      autoComplete="off"
      labelCol={{ span: 0 }}
      wrapperCol={{ span: 24 }}
      onChange={(value, values) => {
        updateStoreData(values.data || []);
      }}
    >
      <Form.List field="data">
        {(fields, { add, remove, move }) => {
          return (
            <div
              style={{
                display: "inline-block",
                paddingLeft: 10,
                paddingTop: 10,
              }}
            >
              {fields.map((item, index) => {
                return (
                  <div key={item.key}>
                    <Form.Item>
                      <Grid.Row style={{ flexWrap: "nowrap" }}>
                        <Grid.Col flex="auto">
                          <Input.Group compact>
                            <Form.Item field={item.field + ".key"} noStyle>
                              <Input style={{ width: "50%" }} title="key" />
                            </Form.Item>
                            <Form.Item field={item.field + ".value"} noStyle>
                              <Input style={{ width: "50%" }} title="value" />
                            </Form.Item>
                          </Input.Group>
                        </Grid.Col>
                        <Grid.Col flex="32px" style={{ margin: "0 2px" }}>
                          <Button
                            icon={<IconDelete />}
                            type="text"
                            status="danger"
                            onClick={() => remove(index)}
                          ></Button>
                        </Grid.Col>
                      </Grid.Row>
                    </Form.Item>
                  </div>
                );
              })}
              <Form.Item>
                <Button
                  onClick={() => {
                    add();
                  }}
                >
                  添加参数
                </Button>
              </Form.Item>
            </div>
          );
        }}
      </Form.List>
    </Form>
  );
};
