import { PreviewData } from "@/api";
import { TreeDataType } from "@arco-design/web-react/es/Tree/interface";

export interface PreviewTreeStore {
  data: PreviewTreeData[];
  selectedKey?: string;
  selectedRecord?: PreviewData;
  originDataList: PreviewData[];
}

export interface PreviewTreeData extends TreeDataType {
  data?: PreviewData;
}
