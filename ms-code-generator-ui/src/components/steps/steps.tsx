import { createAtom, useStore } from "@/util/hooks";
import { Steps } from "@arco-design/web-react";
import { AppStepStore } from "./inteface";

export const AppStepAtom = createAtom<AppStepStore>({
  current: 1,
  id: "db",
});

export const AppSteps = () => {
  const { value, updateStore } = useStore(AppStepAtom);

  return (
    <Steps
      type="arrow"
      current={value.current}
      onChange={(current, id) => {
        updateStore({
          current: current,
          id: id,
        });
      }}
      className={"user-select-none"}
    >
      <Steps.Step
        id="db"
        title="选择表"
        description="选择一个数据源，并从数据源中选择需要生成的表"
      />
      <Steps.Step
        id="template"
        title="选择模版和配置"
        description="选择或创建模版及配置，将根据这些设置生成代码"
      />
      <Steps.Step
        id="preview"
        title="预览及生成"
        description="可以预览生成的结果或直接生成代码"
      />
    </Steps>
  );
};
