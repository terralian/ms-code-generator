export interface AppStepStore {
  current: number;
  id: string;
}
