import { ComponentConfig } from "@arco-design/web-react/es/ConfigProvider/interface";

export const ArcoDesignConfig: ComponentConfig = {
  Form: {
    validateMessages: {
      required: (_, { label }) => {
        return <span>{label} 是必填项</span>;
      },
    },
  },
};
