/**
 * 配置注册接口
 */
export interface IConfigurationRegister {
  /**
   * 注册配置
   */
  register: () => void;
}

/**
 * 配置注册类扫描器
 */
export class ConfigurationRegisterScanner {
  /**
   * 扫描register包下的所有注册接口，并进行调用
   */
  public static async scan() {
    const modules = import.meta.glob("./registers/*.ts", { eager: true });
    for (const path in modules) {
      const module = modules[path] as any;
      const moduleKeys = Object.keys(module);
      for (const moduleKey of moduleKeys) {
        const moduleItem = module[moduleKey];
        if (
          moduleItem.prototype === undefined ||
          moduleItem.prototype.register === undefined
        ) {
          continue;
        }
        moduleItem.prototype.register.call();
      }
    }
  }
}
