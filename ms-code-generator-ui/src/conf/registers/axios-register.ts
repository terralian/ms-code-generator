import { IConfigurationRegister } from "@/conf";
import { Message } from "@arco-design/web-react";
import axios, { AxiosError } from "axios";

export class AxiosRegister implements IConfigurationRegister {
  public register() {
    // axios统一前缀
    axios.defaults.baseURL = import.meta.env.VITE_BASE_API;
    // axios全局异常处理
    axios.interceptors.response.use(
      (k) => k,
      (error: AxiosError) => {
        Message.error(error.message);
      }
    );
  }
}
