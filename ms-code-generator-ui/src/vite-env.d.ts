/// <reference types="vite/client" />
/// <reference types="vite-plugin-svgr/client" />

interface ImportMetaEnv {
  readonly VITE_BASE_API: string;
  readonly VITE_USER_NODE_ENV: "development" | "production";
}
