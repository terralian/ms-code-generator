import axios from "axios";
import { Result, TemplateFile } from "./inteface";

export async function list() {
  const res = await axios.get<Result<TemplateFile[]>>("/template/file/list");
  return res.data;
}

export async function save(data: TemplateFile) {
  const res = await axios.post<Result<any>>("/template/file/save", data);
  return res.data;
}

export async function rename(data: TemplateFile) {
  const res = await axios.put<Result<any>>("/template/file/rename", data);
  return res.data;
}

export async function deleteFile(path: string) {
  const res = await axios.delete<Result<any>>("/template/file/delete", {
    params: {
      path,
    },
  });
  return res.data;
}
