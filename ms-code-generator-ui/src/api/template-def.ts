import axios from "axios";
import { Result, TemplateDef } from "./inteface";

export async function getById(id: string) {
  const res = await axios.get<Result<TemplateDef>>(`/template/def/${id}`);
  return res.data;
}

export async function update(data: TemplateDef) {
  const res = await axios.put<Result<TemplateDef>>(
    `/template/def/update`,
    data
  );
  return res.data;
}
