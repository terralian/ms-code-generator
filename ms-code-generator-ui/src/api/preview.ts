import axios from "axios";
import { Preview, PreviewCallRequest, PreviewData, Result } from "./inteface";

export async function call(params: PreviewCallRequest) {
  const res = await axios.post<Result<Preview>>("/preview/call", params);
  return res.data;
}

export async function getParams(templateIds?: string[]) {
  const res = await axios.post<Result<Record<string, string>>>(
    "/preview/params",
    templateIds || []
  );
  return res.data;
}

export async function openFolder(url: string) {
  const res = await axios.post<Result<Record<string, string>>>(
    "/preview/openFolder",
    { url: url }
  );
  return res.data;
}

export async function generate(baseUrl: string, data: PreviewData[]) {
  const res = await axios.post<Result<object>>("/preview/generate", {
    baseUrl,
    data,
  });
  return res.data;
}
