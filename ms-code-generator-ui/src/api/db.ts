import axios from "axios";
import { ColumnRef, Database, Result, TableRef } from "./inteface";

export async function list() {
  const res = await axios.get<Result<Database[]>>("/db/list");
  return res.data;
}

export async function tables(id: string) {
  const res = await axios.get<Result<TableRef[]>>("/db/tables", {
    params: { id },
  });
  return res.data;
}

export async function columns(id: string, tableName: string) {
  const res = await axios.get<Result<ColumnRef[]>>("/db/columns", {
    params: { id, tableName },
  });
  return res.data;
}

export async function save(db: Database) {
  const res = await axios.post<Result<never>>("/db/save", db);
  return res.data;
}

export async function update(db: Database) {
  const res = await axios.put<Result<never>>("/db/update", db);
  return res.data;
}

export async function connectTest(db: Database) {
  const res = await axios.post<Result<never>>("/db/connect-test", db);
  return res.data;
}

export async function deleteDB(id: string) {
  const res = await axios.delete<Result<never>>("/db/delete", {
    params: { id },
  });
  return res.data;
}
