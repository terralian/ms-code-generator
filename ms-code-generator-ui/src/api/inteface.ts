export interface Result<T> {
  success: boolean;
  message: string;
  data: T;
}

export interface Database {
  id?: string;
  name: string;
  url: string;
  username?: string;
  password?: string;
  driverClassName: string;
  dbName: string;
}

export interface TableRef {
  tableName: string;
  tableComment: string;
}

export interface ColumnRef {
  tableName: string;
  columnName: string;
  ordinalPosition: number;
  isNullable: string;
  dataType: string;
  columnKey: string;
  columnComment: string;
}

export interface TemplateDef {
  id: string;
  baseUrlTemp: string;
  packageNameTemp: string;
  fileNameTemp: string;
  data: string;
}

export interface TemplateFile {
  id: string;
  fileName: string;
  directory: boolean;
  parentId: string;
  path: string;
  level: number;
}

export interface PreviewCallRequest {
  dbId: string;
  tableRefs: TableRef[];
  templateFileIds: string[];
  baseUrl?: string;
  tableNameReplaceIdentifier?: string;
  params: object;
}

export interface Preview {
  baseUrl: string;
  data: PreviewData[];
}

export interface PreviewData {
  absoluteUrl: string;
  parentBaseUrl: string;
  codeBaseUrl: string;
  packageName: string;
  packageUrl: string;
  fileName: string;
  data: string;
}
