import { useEffect } from "react";
import { RecoilState, SetterOrUpdater, atom, useRecoilState } from "recoil";
import { IdWorker } from "../id-worker";

export interface Store<T> {
  /** 状态值 */
  value: T;
  /** 更新，只需要传其中一部分字段 */
  updateStore: (nextStore: Partial<T>) => void;
  /** 更新，需要传整个对象 */
  updateStoreAll: SetterOrUpdater<T>;
}

/**
 * 使用状态管理
 * @param storeAtom 状态原子定义，可以使用createAtom创建
 * @param initialData 延迟初始化的数据，当该值存在时，会立即更新一次
 */
export const useStore = <T>(
  storeAtom: RecoilState<T>,
  initialData?: T
): Store<T> => {
  const [value, setValue] = useRecoilState<T>(storeAtom);

  const updateStore = (nextValue: Partial<T>) => {
    setValue((prev) => ({
      ...prev,
      ...nextValue,
    }));
  };

  useEffect(() => {
    if (initialData) {
      updateStore(initialData);
    }
  }, []);

  return {
    value,
    updateStore,
    updateStoreAll: setValue,
  };
};

/**
 * 创建状态，其key通过uuid初始化
 * @param data 初始状态
 */
export const createAtom = <T>(data: T) => {
  return atom({
    key: IdWorker.uuid(),
    default: data,
  });
};
