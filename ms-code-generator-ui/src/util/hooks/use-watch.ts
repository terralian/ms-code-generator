import { Form, FormInstance } from "@arco-design/web-react";

/**
 * 封装Form.useWatch，修复其在ts校验上的不友好问题
 * @param field 字段名
 * @param form 表单入口
 */
export const useWatch = <FormField, FieldType = any>(
  field: string,
  form?: FormInstance<FormField>
): FieldType => {
  return Form.useWatch(field, form as unknown as FormInstance);
};
