import { v4 as uuid } from "uuid";
import SnowFlake from "./snow-flake";

/**
 * 随机主键生成器
 */
export class IdWorker {
  private static sf = new SnowFlake(0n, 0n);

  /**
   * 生成UUID
   */
  public static uuid() {
    return uuid();
  }

  /**
   * 生成雪花算法主键
   */
  public static snowFlake() {
    return this.sf.nextId();
  }
}
