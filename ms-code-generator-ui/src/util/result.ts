import { Result } from "@/api";
import { Message } from "@arco-design/web-react";

export function showResult(res: Result<any>) {
  if (res.success) {
    Message.success(res.message || "操作成功");
  } else {
    Message.error(res.message || "操作失败");
  }
  return res.success;
}

export function showError(res: Result<any>) {
  if (!res.success) {
    Message.error(res.message);
  }
  return res.success;
}
