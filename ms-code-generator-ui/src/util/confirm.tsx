import { Modal, ModalProps } from "@arco-design/web-react";
import "@/components/modal/base-modal.less";

export function removeConfirm(onOk: ModalProps["onOk"]) {
  Modal.confirm({
    className: "base-modal",
    alignCenter: false,
    title: "是否确认删除?",
    maskStyle: {
      backgroundColor: "transparent",
      transition: "none",
    },
    onOk: onOk,
  });
}
