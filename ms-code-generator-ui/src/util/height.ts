export const fillHeightStyle = (level: number) => {
  switch (level) {
    case 1:
      return `calc(-72px + 100vh)`;
    case 2:
      return `calc(-115px + 100vh)`;
  }
};
