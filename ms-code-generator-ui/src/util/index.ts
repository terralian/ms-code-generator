// -------------------------------------
// 工具包，可在各层使用的工具类
// -------------------------------------

export { IdWorker } from "./id-worker";
export * as ResultUtil from "./result";
export * as ConfirmUtil from "./confirm";
export * as HeightUtil from "./height";
