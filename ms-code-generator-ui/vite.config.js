import { defineConfig, loadEnv } from "vite";
import { resolve } from "path";
import react from "@vitejs/plugin-react";
import { getProxy } from "./proxy";

const config = ({ mode }) => {
  // 读取环境配置
  const env = loadEnv(mode, process.cwd());

  return defineConfig({
    plugins: [react()],
    base: "./",
    define: {
      "process.env.NODE_ENV": `"${mode}"`,
    },
    resolve: {
      alias: {
        "@": resolve(__dirname, "src"),
      },
    },
    server: {
      proxy: getProxy(env),
    },
  });
};

export default config;
